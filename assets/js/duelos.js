$(document).ready(function(){
    setTimeout(function(){ 
        $(".active").removeClass("active");
        $("#menuduelos").addClass("active");
    }, 300);
    $("#btnAgregarTorneo").click(function(){
        window.location.href = "formulariotorneo.html?opcion=crear";
    });
    //se cargan torneos disponibles
    if(torneos.length>0){//si existen rides estonces se cargan
        var torneosDisponibles = '';
        torneos.forEach(function(value, index, array) {
            torneosDisponibles += '<div class="row mb-5 card text-white bg-secondary text-center">'+
                '<div class="card-header">'+array[index][1]+'</div>'+
                '<div class="card-body">'+
                '<div class="d-flex justify-content-around">'+
                '<div><img class="img_duelos" src="assets/img/'+array[index][2]+'.jpg"></div>'+
                
                '</div>'+
                '<h5 class="card-title">'+array[index][2]+'</h5>'+
                '<p class="card-text">'+array[index][4]+'.</p>'+
                '<a href="#" id="btnNuevoDuelo" onclick="nuevoDuelo('+array[index][0]+',\''+array[index][2]+'\')" class="btn btn_duelo btn-primary">Nuevo Duelo</a>'+
                '</div>'+
                '<div class="card-footer">Recompensa: '+array[index][3]+' Puntos</div>'+
            '</div>';
        });
        document.getElementById("contenedorTorneosDisponibles").innerHTML = torneosDisponibles;
    }
    //fin torneos disponibles
    if(duelos.length>0){//si existen duelos estonces se cargan
        var tabla = '<table>'+
            '<tr>'+
                '<th>Número de duelo</th>'+
                '<th>Nombre Torneo</th>'+
                '<th>Juego</th>'+
                '<th>Jugadores</th>'+
                '<th>Recompensa</th>'+
                '<th>Estado</th>'+
                '<th>Ganador</th>'+
            '</tr>';
        duelos.forEach(function(value, index, array) {
            //verificamos el torneo
            var nombreTorneo = "";
            var nombreJuego = "";
            var recompensaTorneo = 0;
            var nombrejugador1 = "";
            var nombrejugador2 = "";
            var ganador = "";
            torneos.forEach(function(value1, index1, array1) {
                if(array[index][3] == array1[index1][0]){
                    nombreTorneo =  array1[index1][1];
                    recompensaTorneo =  array1[index1][3];
                    nombreJuego = array1[index1][2];
                }
            });
            usuarios.forEach(function(value2, index2, array2) {
                if(array[index][1] == array2[index2][0]){
                    nombrejugador1 = array2[index2][3];
                }
                if(array[index][2] == array2[index2][0]){
                    nombrejugador2 = array2[index2][3];
                }
                if(array[index][5] != "N/A"){
                    if(array[index][5] == array2[index2][0]){
                        ganador = array2[index2][3];
                    }
                }
            });
            tabla += '<tr>'+
                    '<td>'+array[index][0]+'</td>'+
                    '<td>'+nombreTorneo+'</td>'+
                    '<td>'+nombreJuego+'</td>'+
                    '<td>'+nombrejugador1+' VS '+nombrejugador2+'</td>'+
                    '<td>'+recompensaTorneo+'</td>';
                    if(array[index][4] == "Pendiente"){
                        tabla += '<td>'+array[index][4]+' <i class="fa fa-clock"></i></td>';
                    }else{
                        tabla += '<td>'+array[index][4]+' <i class="fa fa-check-square"></i></td>';
                    }
            tabla += '<td>';
                if(ganador == ""){
                    tabla += '<select class="select_ganadores" name="'+array[index][0]+'||'+array[index][3]+'"><option value="0">Seleccionar Ganador</option><option value="'+array[index][1]+'">'+nombrejugador1+'</option><option value="'+array[index][2]+'">'+nombrejugador2+'</option></select>';
                }else{
                    tabla += ganador;
                }
            tabla += '</td>'+
                '</tr>';
        });
        tabla += '</table>';
        document.getElementById("contenedorDuelos").innerHTML = tabla;
        $(".select_ganadores").change(function(){
            if(idLogueado == 0){
                var r = confirm("Seguro que desea asignar el ganador del duelo?");
                if(r == true){
                    var datos = $(this).attr("name").split("||");
                    var torneoNum = datos[1];
                    var dueloNum = datos[0];
                    var ganadorduelo = $(this).val();
                    var recom = 0;
                    torneos.forEach(function(value1, index1, array1) {
                        if(torneoNum == array1[index1][0]){
                            recom = array1[index1][3];
                        }
                    });
                    usuarios.forEach(function(value, index, array) {
                        if(ganadorduelo == array[index][0]){
                            var puntosActuales = parseInt(array[index][6]);
                            array[index][6] = puntosActuales + parseInt(recom);
                        }
                    });
                    duelos.forEach(function(value2, index2, array2) {
                        if(dueloNum == array2[index2][0]){
                            array2[index2][4] = "Finalizado";
                            array2[index2][5] = ganadorduelo;
                        }
                    });
                    localStorage.setItem('duelos', JSON.stringify(duelos));
                    localStorage.setItem('usuarios', JSON.stringify(usuarios));
                    alert("Ganador asignado con éxito!");
                    window.location.href = "duelos.html";
                }else{
                    $(this).val(0);
                }
            }else{
                $(this).val(0);
                alert("No puede realizar esta accion");
            }
        });
    }
    $('.btn_duelo').click(function(e) {
        e.preventDefault();
    });
});

function nuevoDuelo(nTorneo,nomJuego){
    if(idLogueado == 0){
        var jugadores = [];
        usuarios.forEach(function(value, index, array) {
            if(array[index][0] != 0){
                if(nomJuego == array[index][4]){
                    jugadores.push(array[index][0]);
                }
            }
        });
        if(jugadores.length >1){
            var jugador1 = jugadores[Math.floor(Math.random() * jugadores.length)];
            var jugador2 = jugadores[Math.floor(Math.random() * jugadores.length)];
            while(jugador1 == jugador2){
                jugador2 = jugadores[Math.floor(Math.random() * jugadores.length)];
            }
            var dueloNuevo = [];
            dueloNuevo[0] = idDuelo;
            dueloNuevo[1] = jugador1;
            dueloNuevo[2] = jugador2;
            dueloNuevo[3] = nTorneo;
            dueloNuevo[4] = "Pendiente";
            dueloNuevo[5] = "N/A";
            duelos.push(dueloNuevo);
            //realizar registro
            localStorage.setItem('duelos', JSON.stringify(duelos));
            alert("Duelo realizado con éxito!");
            window.location.href = "duelos.html";
        }else{
            alert("No hay suficientes jugadores de "+nomJuego);
        }
    }else{
        alert("Accion permitida solamente por el administrador");
    }
    
}
