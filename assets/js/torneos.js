$(document).ready(function(){
    $("#btnAgregarTorneo").click(function(){
        if(idLogueado == 0){
            window.location.href = "formulariotorneo.html?opcion=crear";
        }else{
            alert("No tiene permisos");
        }
        
    });
    setTimeout(function(){ 
        $(".active").removeClass("active");
        $("#menutorneos").addClass("active");
    }, 300);
    
    if(torneos.length>0){//si existen torneos estonces se cargan
        var tabla = '<table>'+
            '<tr>'+
                '<th>Número de torneo</th>'+
                '<th>Nombre Torneo</th>'+
                '<th>Juego</th>'+
                '<th>Recompensa</th>'+
                '<th>Descripcion</th>'+
                '<th>Editar</th>'+
                '<th>Borrar</th>'+
            '</tr>';
        torneos.forEach(function(value, index, array) {
            tabla += '<tr>'+
                    '<td>'+array[index][0]+'</td>'+
                    '<td>'+array[index][1]+'</td>'+
                    '<td>'+array[index][2]+'</td>'+
                    '<td>'+array[index][3]+'</td>'+
                    '<td>'+array[index][4]+'</td>'+
                    '<td><img onclick="editarTorneo('+array[index][0]+')" class="btnTablaTorneo" src="assets/img/editar.png" alt=""></td>'+
                    '<td><img onclick="eliminarTorneo('+array[index][0]+')" class="btnTablaTorneo" src="assets/img/eliminar.png" alt=""></td>'+
                '</tr>';
        });
        tabla += '</table>';
        document.getElementById("contenedorTorneos").innerHTML = tabla;
    }
});

function editarTorneo(id){
    if(idLogueado == 0){
        window.location.href = "formulariotorneo.html?opcion=editar&id="+id;
    }else{
        alert("No tiene permisos");
    }
}

function eliminarTorneo(id){
    if(idLogueado == 0){
        var r = confirm("Seguro que desea eliminar el torneo seleccionado?");
        if(r == true){
            torneos.forEach(function(value, index, array) {
                if(id == array[index][0]){
                    array.splice(index, 1);
                }
            });
            localStorage.setItem('torneos', JSON.stringify(torneos));
            alert("Torneo eliminado con éxito!");
            window.location.href = "torneos.html";
        }
    }else{
        alert("No tiene permisos");
    }
}