var idTorneoActualizar = 0;
var torneoActualizar = [];
if(idLogueado !=0){ //si no es admin
    window.location.href = "index.html";
}
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const opcion = urlParams.get('opcion');
if(opcion == "crear"){
    document.getElementById("tituloformTorneo").innerHTML = 'Crear Torneo';
}else if(opcion == "editar"){
    document.getElementById("tituloformTorneo").innerHTML = 'Modificar Torneo';
    $("#btnCrearTorneo").html("Actualizar Torneo");
    //cargar datos de ride para modificar
    idTorneoActualizar = urlParams.get('id');
    torneos.forEach(function(value, index, array) {
        if(idTorneoActualizar == array[index][0]){
            $("#nombretorneo").val(array[index][1]);
            $("#juego").val(array[index][2]);
            $("#recompensas").val(array[index][3]);
            $("#descripcion").val(array[index][4]);
        }
    });
}
$(document).ready(function(){
    $("#btnCrearTorneo").click(function(){
        if(idTorneoActualizar != 0){
            guardarTorneo("actualizar");
        }else{
            guardarTorneo("crear");
        }
    });
});

function guardarTorneo(opcion){
    document.getElementById("datos_requeridos").innerHTML = "";
    var nombretorneo = $("#nombretorneo").val();
    var juego = $("#juego").val();
    var recompensas = $("#recompensas").val();
    var decripcion = $("#descripcion").val();
    var paso = 1;
    //validar nombre
    if(nombretorneo.length <= 0){
        $("#nombretorneo").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "*Debe de ingresar nombre de torneo";
        paso = 0;
    }else{
        $("#nombretorneo").removeClass("inputerror");
    }
    //validar juego
    if(juego == 0){
        $("#juego").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe seleccionar un juego";
        paso = 0;
    }else{
        $("#juego").removeClass("inputerror");;
    }
    //validar recompensas
    if(recompensas.length <= 0){
        $("#recompensas").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe de ingresar un email";
        paso = 0;
    }else{
        $("#recompensas").removeClass("inputerror");
    }
    if(paso == 1){
        if(opcion == "crear"){
            var datosTorneo = [];
            datosTorneo[0] = idTorneo;
            datosTorneo[1] = nombretorneo;
            datosTorneo[2] = juego;
            datosTorneo[3] = recompensas;
            datosTorneo[4] = decripcion;
            torneos.push(datosTorneo);
            //realizar registro
            localStorage.setItem('torneos', JSON.stringify(torneos));
            alert("Datos guardados con éxito!");
            window.location.href = "torneos.html";
        }else{//actualizar
            torneoActualizar = [];
            torneoActualizar[0] = idTorneoActualizar;
            torneoActualizar[1] = nombretorneo;
            torneoActualizar[2] = juego;
            torneoActualizar[3] = recompensas;
            torneoActualizar[4] = decripcion;
            torneos.forEach(actualizarDatosTorneo);
            //realizar update
            localStorage.setItem('torneos', JSON.stringify(torneos));
            alert("Datos guardados con éxito!");
            window.location.href = "torneos.html";
        }
    }
}
function actualizarDatosTorneo(value, index, array) {
    if(value[0] == idTorneoActualizar){//actualizar datos en el array de torneos
        array[index][0] = torneoActualizar[0];
        array[index][1] = torneoActualizar[1];
        array[index][2] = torneoActualizar[2];
        array[index][3] = torneoActualizar[3];
        array[index][4] = torneoActualizar[4];
    }
}