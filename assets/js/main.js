$("#footer").load("includes/footer.html");
//se cargan usuarios del sistema
var idUsuario = 1;
var idTorneo = 1;
var idDuelo = 1;
//cargar datos del sistema
var usuarios = [];
var torneos = [];
var duelos = [];
var usuariosLocal = JSON.parse(localStorage.getItem('usuarios'));
var torneosLocal = JSON.parse(localStorage.getItem('torneos'));
var duelosLocal = JSON.parse(localStorage.getItem('duelos'));
//cargar torneos
if(torneosLocal == null){
  //torneos no definido
}else{
  //si existe un torneo almenos
  torneos = torneosLocal;
  torneos.forEach(function(value, index, array) {
      idTorneo = array[index][0];
  });
  idTorneo = idTorneo + 1;
}
//cargar duelos
if(duelosLocal == null){
  //duelos no definido
}else{
  //si existe un duelo almenos
  duelos = duelosLocal;
  duelos.forEach(function(value, index, array) {
      idDuelo = array[index][0];
  });
  idDuelo = idDuelo + 1;
}
//cargar usuarios
var idLogueado = sessionStorage.getItem("usuarioLogueado");
var usuarioLogueado = [];
function wait(ms){
  var start = new Date().getTime();
  var end = start;
  while(end < start + ms) {
    end = new Date().getTime();
 }
}
wait(700);
if(usuariosLocal == null){
    //usuario no definido
    var usuarioAdmin = [];
    usuarioAdmin[0] = 0;
    usuarioAdmin[1] = "Administrador";
    usuarioAdmin[2] = "admin@competitivecr.co.cr";
    usuarioAdmin[3] = "admin";
    usuarioAdmin[4] = "Fortnite";
    usuarioAdmin[5] = "admin";
    usuarioAdmin[6] = 0;
    usuarios.push(usuarioAdmin);
    //realizar registro de usuario admin
    localStorage.setItem('usuarios', JSON.stringify(usuarios));
  }else{
    //si existe un usuario
    usuarios = usuariosLocal;
    usuarios.forEach(function(value, index, array) {
        idUsuario = array[index][0];
    });
    idUsuario = idUsuario + 1;
}
$("#header").load("includes/header.html",function(){
    var headeralto = $("header").height();
    $(".containerprincipal").css("margin-top",headeralto);
    if(idLogueado != null){
      usuarios.forEach(verificarSesionUsuarios);
    }
});

function verificarSesionUsuarios(value, index, array) {
  if(value[0] == idLogueado){//usuario logueado entonces se cargan datos
    $("#menu_perfil").addClass("drop-down");
      document.getElementById("menu_perfil").innerHTML = '<a class="perfil" href="empezar.html">Perfil</a>'+
          '<ul>'+
              '<li id="cerrarsesion">'+
                  '<a href="#">Cerrar Sesión</a>'+
              '</li>'+
          '</ul>';
      $(".perfil").html(array[index][3]);
      document.getElementById("cerrarsesion").onclick = function() { cerrarSesion(); }
      usuarioLogueado[0] = array[index][0];
      usuarioLogueado[1] = array[index][1];
      usuarioLogueado[2] = array[index][2];
      usuarioLogueado[3] = array[index][3];
      usuarioLogueado[4] = array[index][4];
      usuarioLogueado[5] = array[index][5];
      usuarioLogueado[6] = array[index][6];
  }
}
// '<li class="mb-2" id="misPremios">'+
//                   '<a href="mispremios.html">Mis Premios</a>'+
//               '</li>'+
//funcion de cerrar sesion
function cerrarSesion(){
    sessionStorage.removeItem("usuarioLogueado");
    window.location.href = "index.html";
}

//***************
!(function($) {
  "use strict";
  
  
  // Smooth scroll for the navigation menu and links with .scrollto classes
  $(document).on('click', '.nav-menu a, .mobile-nav a, .scrollto', function(e) {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      e.preventDefault();
      var target = $(this.hash);
      if (target.length) {

        var scrollto = target.offset().top;
        var scrolled = 20;

        if ($('#header').length) {
          scrollto -= $('#header').outerHeight()

          if (!$('#header').hasClass('header-scrolled')) {
            scrollto += scrolled;
          }
        }

        if ($(this).attr("href") == '#header') {
          scrollto = 0;
        }

        $('html, body').animate({
          scrollTop: scrollto
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.nav-menu, .mobile-nav').length) {
          $('.nav-menu .active, .mobile-nav .active').removeClass('active');
          $(this).closest('li').addClass('active');
        }

        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
          $('.mobile-nav-overly').fadeOut();
        }
        return false;
      }
    }
  });

  // Mobile Navigation
  if ($('.nav-menu').length) {
    var $mobile_nav = $('.nav-menu').clone().prop({
      class: 'mobile-nav d-lg-none'
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" class="mobile-nav-toggle d-lg-none"><i class="icofont-navigation-menu"></i></button>');
    $('body').append('<div class="mobile-nav-overly"></div>');

    $(document).on('click', '.mobile-nav-toggle', function(e) {
      $('body').toggleClass('mobile-nav-active');
      $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
      $('.mobile-nav-overly').toggle();
    });

    $(document).on('click', '.mobile-nav .drop-down > a', function(e) {
      e.preventDefault();
      $(this).next().slideToggle(300);
      $(this).parent().toggleClass('active');
    });

    $(document).click(function(e) {
      var container = $(".mobile-nav, .mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('icofont-navigation-menu icofont-close');
          $('.mobile-nav-overly').fadeOut();
        }
      }
    });
  } else if ($(".mobile-nav, .mobile-nav-toggle").length) {
    $(".mobile-nav, .mobile-nav-toggle").hide();
  }

  // Navigation active state on scroll
  var nav_sections = $('section');
  var main_nav = $('.nav-menu, #mobile-nav');

  $(window).on('scroll', function() {
    var cur_pos = $(this).scrollTop() + 80;

    nav_sections.each(function() {
      var top = $(this).offset().top,
        bottom = top + $(this).outerHeight();

      if (cur_pos >= top && cur_pos <= bottom) {
        if (cur_pos <= bottom) {
          //main_nav.find('li').removeClass('active');
        }
        main_nav.find('a[href="#' + $(this).attr('id') + '"]').parent('li').addClass('active');
      }
      if (cur_pos < 300) {
        //$(".nav-menu ul:first li:first").addClass('active');
      }
    });
  });

  // Toggle .header-scrolled class to #header when page is scrolled
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('#header').addClass('header-scrolled');
    } else {
      $('#header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 100) {
    $('#header').addClass('header-scrolled');
  }
})(jQuery);