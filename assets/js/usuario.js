$(document).ready(function(){
    $("#btnregistrar").click(function(){
        $("#contenedor_empezar").load("registro.html");
    });
    $("#btniniciar").click(function(){
        $("#contenedor_empezar").load("login.html");
    });
    $("#btnCrear").click(function(){
        if(idLogueado != null){
            guardarUsusario("actualizar");
        }else{
            guardarUsusario("crear");
        }
    });
    $("#btnIniciar").click(function(){
        iniciarSesion();
    });
});
var usuarioActualizar = [];
function guardarUsusario(opcion){
    document.getElementById("datos_requeridos").innerHTML = "";
    var nombre = $("#nombre").val();
    var favorito = $("#favorito").val();
    var email = $("#email").val();
    var usuario = $("#usuario").val();
    var pass = $("#password").val();
    var confirmarpass = $("#confirmarpass").val();
    var paso = 1;
    //validar nombre
    if(nombre.length <= 0){
        $("#nombre").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "*Debe de ingresar su nombre";
        paso = 0;
    }else{
        $("#nombre").removeClass("inputerror");
    }
    //validar nombre de usuario
    if(usuario.length <= 0 || (usuario == "admin" && idLogueado != 0)){
        $("#usuario").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe de ingresar un nombre de usuario valido";
        paso = 0;
    }else{
        $("#usuario").removeClass("inputerror");;
    }
    //validar email
    if(email.length <= 0){
        $("#email").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe de ingresar un email";
        paso = 0;
    }else{
        $("#email").removeClass("inputerror");
    }
    //validar favorito
    if(favorito == 0){
        $("#favorito").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe seleccionar su juego favorito";
        paso = 0;
    }else{
        $("#favorito").removeClass("inputerror");
    }
    //validar pass
    if(pass != confirmarpass || pass.length == 0){
        $("#password").addClass("inputerror");
        $("#confirmarpass").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Verificar contraseña";
        paso = 0;
    }else{
        $("#password").removeClass("inputerror");
        $("#confirmarpass").removeClass("inputerror");
    }
    if(paso == 1){
        if(opcion == "crear"){
            var datosusuario = [];
            datosusuario[0] = idUsuario;
            datosusuario[1] = nombre;
            datosusuario[2] = email;
            datosusuario[3] = usuario;
            datosusuario[4] = favorito;
            datosusuario[5] = pass;
            datosusuario[6] = 0;
            usuarios.push(datosusuario);
            //realizar registro
            localStorage.setItem('usuarios', JSON.stringify(usuarios));
            alert("Datos guardados con éxito!");
            window.location.href = "empezar.html";
        }else{//actualizar
            usuarioActualizar = [];
            usuarioActualizar[0] = idLogueado;
            usuarioActualizar[1] = nombre;
            usuarioActualizar[2] = email;
            usuarioActualizar[3] = usuario;
            usuarioActualizar[4] = favorito;
            usuarioActualizar[5] = pass;
            usuarios.forEach(actualizarDatosUsuario);
            //realizar update
            localStorage.setItem('usuarios', JSON.stringify(usuarios));
            alert("Datos guardados con éxito!");
            window.location.href = "empezar.html";
        }
    }
}
function actualizarDatosUsuario(value, index, array) {
    if(value[0] == idLogueado){//usuario logueado entonces se actualizan datos
        array[index][0] = usuarioActualizar[0];
        array[index][1] = usuarioActualizar[1];
        array[index][2] = usuarioActualizar[2];
        array[index][3] = usuarioActualizar[3];
        array[index][4] = usuarioActualizar[4];
        array[index][5] = usuarioActualizar[5];
    }
}
//funcion para iniciar sesion
function iniciarSesion(){
    var cantidadUsuarios = usuarios.length;
    document.getElementById("datos_requeridos").innerHTML = "";
    var paso = 1;
    var usuarioInicio = $("#usuario").val();
    //validar nombre de usuario
    if(usuarioInicio.length <= 0){
        $("#usuario").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe de ingresar el nombre de usuario";
        paso = 0;
    }else{
        $("#usuario").removeClass("inputerror");
    }
    var passInicio = $("#password").val();
    //validar clave
    if(passInicio.length <= 0){
        $("#password").addClass("inputerror");
        document.getElementById("datos_requeridos").innerHTML += "<br>*Debe de ingresar la contraseña";
        paso = 0;
    }else{
        $("#password").removeClass("inputerror");
    }
    if(cantidadUsuarios == 0){
        alert("Usuario/contraseña incorrecto");
        return;
    }
    if(paso == 1){
        try {
            var cantidad = usuarios.length - 1;
            var loguea = 0;
            usuarios.forEach(function(value, index, array) {
                if(value[3] == usuarioInicio && value[5] == passInicio){//usuario logueado entonces se cargan datos
                    loguea = 1;
                    sessionStorage.setItem('usuarioLogueado', value[0]);
                    window.location.href = "empezar.html";
                }
                if (cantidad === index) throw BreakException;
            });
        } catch (e) {
            if(loguea == 0){
                alert("Usuario/contraseña incorrecto");
            }
        }
    }
}