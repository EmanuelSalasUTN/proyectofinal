setTimeout(function(){ 
    $(".active").removeClass("active");
    $("#menucontacto").addClass("active");
}, 500);
// Initialize and add the map
function initMap() {
// The location of Uluru
var uluru = {lat: 10.4699606, lng: -84.6408713};
// The map, centered at Uluru
var map = new google.maps.Map(
    document.getElementById('map'), {zoom: 4, center: uluru});
// The marker, positioned at Uluru
var marker = new google.maps.Marker({position: uluru, map: map});

}
initMap();
